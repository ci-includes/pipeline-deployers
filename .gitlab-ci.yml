# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2024 Olliver Schinagl <oliver@schinagl.nl>

include:
  # Do not use $CI_COMMIT_SHA in any project, this is for testing here only!
  - component: $CI_SERVER_HOST/$CI_PROJECT_PATH/release-brancher@$CI_COMMIT_SHA
  - component: $CI_SERVER_HOST/$CI_PROJECT_PATH/release-announcer@$CI_COMMIT_SHA
    inputs:
      release_assets: 'assets/release.json'
      release_delay: '1 sec'
  - project: 'ci-includes/masslinter'
    ref: 'master'
    file: 'all-linters.yml'

workflow:
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - if: '$CI_COMMIT_BRANCH =~ /^release\/v\d+.\d+$/'
    - if: '$CI_COMMIT_TAG'
    - if: '$CI_MERGE_REQUEST_IID'
    - if: '$CI_PIPELINE_SOURCE == "web"'

stages:
  - lint
  - branch
  - deploy
  - complete

default:
  tags:
    - docker

deploy:
  image: "index.docker.io/curlimages/curl"
  stage: deploy
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+.*$/'
  artifacts:
    paths:
      - 'assets/'
  before_script:
    - mkdir -p 'assets/'
    - printf '[' > 'assets/release.json'
  after_script:
    - printf ']' >> 'assets/release.json'
  script:
    - |
      _upload_file='README.md'
      _file_name='README'
      _file_path='RELEASE-README.md'
      _url="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/release-announcer/${CI_COMMIT_TAG#v}/${_file_name}"
      curl \
           --fail \
           --header 'Content-Type: application/json' \
           --header "Job-Token: ${CI_JOB_TOKEN}" \
           --output '/dev/null' \
           --request 'PUT' \
           --retry 3 \
           --show-error \
           --silent \
           --upload-file "${_upload_file}" \
           --write-out "Uploaded file: '${_upload_file}'; HTTP response: %{http_code}\n\n" \
           "${_url}"
      printf "{\"name\":\"%s\",\"url\":\"%s\",\"filepath\":\"/%s\"}" "${_file_name}" "${_url}" "${_file_path}" | \
      tee -a 'assets/release.json'
