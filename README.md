# Pipeline deployers, a set of GitLab deploy components
A few gitlab components to help with deployment related tasks, such as creating
a release branch and to create/announce the release of such a tag (using
[GitLab releases][gitlabrelease]).

For users that can/want not update to components (yet), the old stuff is still
available on the git hash, and release branch `relase/v0.0` with git tag
`v0.0.0`.


## Release branches
When working with git, a few git-flows speak of using release branches.
The `release-branch.yml` template offers just that. For more information see
the [release-brancher.md](templates/release-brancher/README.md) documentation.


## Release announcement
To help with announcing a release using [GitLab's releases][gitlabrelease] a
simple template was added to help reduce some boilerplate. Once this feature is
more mature on GitLab's side, this template should not be needed anymore. For
more information see [announce-release.md](templates/release-announcer/README.md).


[gitlabrelease]: https://docs.gitlab.com/ee/ci/yaml/index.html#release
