# Release brancher
It is fairly common with various git strategies to have release branches.
While the action is simple, to just create a branch, this template helps
to automate things by only pushing a specific git tag, and thus the whole
process becomes automated. Automation may be needed, in that a first tag
on this release branch is desired (usually the first release-candidate).

There's several ways to do this, using git commands or the GitLab API.
The advantage of using the GitLab API is that there's no additional data
traffic (no extra git-pulls needed on shallow clones for example) and
authentication is easier handled with tokens as well.

Besides creating a release branch, by default an accompanying release candidate
tag is created, resulting in the following

```console
master
├── v1.0 (release branch tag)
└── release/v1.0 (release branch)
    └────── v1.0.0-rc1 (release candidate tag)
```

The reasoning behind these are the following. To very clearly indicate and
'fixate' the release branch point creation adding traceability.
The release branch in itself is used allow for potential evolution of releases
within that branch its scope. The used `release/` prefix is configurable but
helps with identifying release branches and grouping them in a familiar folder
like structure as is somewhat common with git branching.
Finally when creating a release branch, the intend is usually to prepare for
a release, and thus the first release candidate of that series. A flag exists
to control this behavior.


[TOC]


## Installation
To make use of the GitLab component, it needs to be first included.

```yaml
include:
  - component: gitlab.com/ci-includes/pipeline-deployers/release-brancher@<VERSION>
```

The job itself is triggered by a so called release tag. A release tag is
defined by the **major** and **minor** components of a version. This is done
through a GitLab rule matching a regular expression and part of the include.

In addition to the above, a stage called `deploy` is needed which will cause
this job to run.

The release tag is in this example fixed using a regular expression, matching
for example `v0.1`.


### Runner tags
None of the KiCi jobs configure `tags`, but do depend on `docker` as the
[KiBot] container is used. Since the runner tag is very setup specific, either
allow the desired runners to pick up `any` job, or set the default tag, which
does mean that this introduces less-desirable behavior for any other jobs in
the pipeline.

When using the runner tag `docker` for example, this would look as such.
```yaml
default:
  tags:
    - docker
```


### Dependencies
Because of gitlab-org/gitlab#439524, it is not possible to unset `needs` via an
component input. Thus for now, the only reliable way is to use the `stage`. It
is therefore required to define a stage `branch` (or whatever name is desired
and set it via `inputs.stage_branch` which follows anything of any previous
stages. On fully DAG-ified pipelines, `branch` would have to run after `test`.


### Inputs
See the description field in the [`template`](templates/release-brancher/template.yml)
for details and defaults of supported inputs.


### Variables
#### RELEASE_BRANCHER_TOKEN
The variable `${RELEASE_BRANCHER_TOKEN}` must contain the API token to
authenticate against the API. Also known as the HTTP header
`Authorization: Bearer`.

If unset, the variable `${CI_PERSONAL_TOKEN}` is used, and if that is also
not available the job will fail to execute.

__default: null__


## Generate tagging tips
When wanting to put markdown in git tags, for example to store a changelog,
using markdown may be problematic, due to git by default treating the `#`
character as comment. The combination of the following git options are useful
in that case.

First, the `git commit --cleanup` option can be set to `scissors` via
`git config --global commit.cleanup scissors`.
Secondly, changing the git comment character so that git does not swallow
lines starting with a `#` by setting the commentChar. Any character can be
used, but `;` is recommended here, as this is not a likely character to end up
on the first column. This can be done via
`git config --global core.commentChar ";"`
