# Release announcer
GitLab's release feature and API are a powerful way to inform the world that
a release is ready for consumption. In theory GitLab supports releases via
the [release keyword][releasekeyword] but it is a bit of a kludge at this
moment, and cannot be used with annotated git tag messages as description.

This component does focus on a Tag based release. E.g. all data and information
is expected to be supplied via a git tag.

To circumvent these issues, this template exists, hopefully only temporally,
but expected to last a few years.


[TOC]


## Installation
To make use of the GitLab template, it needs to be first included.

```yaml
include:
  - component: gitlab.com/ci-includes/pipeline-deployers/release-brancher@<VERSION>
```

The job itself is triggered by a so called release tag. A release tag is
defined by the **major**, **minor** and **patch** components of a version.
This is done through a GitLab rule matching a regular expression and part of
the include.

In addition to the above, a stage called `complete` is needed which will cause
this job to run. The stage name is called `complete` and not `deploy` to offer
the option to ensure the announcement of the release is done after any `deploy`
job. E.g. to upload files somewhere. See the section below to learn more about
dependencies.

The release tag is in this example fixed using a regular expression, matching
for example `v0.1.2` at the minimum.


### Assets
Releases can contain additional assets, e.g. files part of the actual release.
However release-cli/release-announcer do not generate or supply these assets
nor their links. A previous job in the pipeline is responsible for ensuring
the assets are stored somewhere. To inform this job of which assets to link
to a release, the following json array is required and the file name is supplied
via `release_assets`. For details check the [GitLab Release API][releasekeyword].

```json
[
  {
    "name": "Asset1",
    "url":"https://example.com/some/location/1",
    "filepath": "abc",
    "link_type": "other"
  }, {
    "name": "Asset2",
    "url":"https://example.com/some/location/2",
    "filepath": "xzy",
    "link_type": "other"
  }
]
```


### Dependencies
This component may depend on a previous job before actually announcing a
release. Most common/often this is after the build assets have been verified
and uploaded somewhere. More complex cases, depending on different projects
or pipelines is not supported from the component itself, but this can easily
be handled in the `needs` of the dependent job itself.


### Runner tags
None of the KiCi jobs configure `tags`, but do depend on `docker` as the
[KiBot] container is used. Since the runner tag is very setup specific, either
allow the desired runners to pick up `any` job, or set the default tag, which
does mean that this introduces less-desirable behavior for any other jobs in
the pipeline.

When using the runner tag `docker` for example, this would look as such.
```yaml
default:
  tags:
    - docker
```


## Inputs
See the description field in the [`template`](templates/release-announcer/template.yml)
for details and defaults of supported inputs.


## Release announcement delay
Release announcements are by default delayed by 86 minutes. While the time is
[arbitrarily][86ed] chosen, the reason it exists is to have a final window of
opportunity to cancel a potentially 'gone-bad' release. As no variables can
be set or used at this level of a job, the only way is to extend the
`announce_release` job and change or remove the delay from the rule.


[releasekeyword]: https://docs.gitlab.com/ee/ci/yaml/index.html#release
[86ed]: https://en.wikipedia.org/wiki/86_(term)
